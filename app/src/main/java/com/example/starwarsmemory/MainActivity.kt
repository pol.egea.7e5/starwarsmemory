package com.example.starwarsmemory

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    var inte:Intent?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        val difficulty = arrayOf<String?>("Easy", "Normal", "Survival")
        setTheme(R.style.Theme_StarWarsMemory_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val inten = Intent(this, GameScreen::class.java)
        inte =inten
        val playClick = findViewById<Button>(R.id.playButton)
        playClick.setOnClickListener {
            inte?.putExtra("time",60)
            startActivity(inte)
        }
        val helpclick = findViewById<Button>(R.id.helpButton)
        helpclick.setOnClickListener {
            val intent = Intent(this, HelpScreen::class.java)
            startActivity(intent)
        }

        val spin = findViewById<Spinner>(R.id.selectorDificultat)
        spin.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                (parentView.getChildAt(0) as? TextView)?.setTextColor(Color.YELLOW)
                inte?.putExtra("dificultat",(parentView.getChildAt(0) as? TextView)?.text as? String)
                Toast.makeText(applicationContext,
                    difficulty[position],
                    Toast.LENGTH_LONG)
                    .show()
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
            }
        }

        // Create the instance of ArrayAdapter
        // having the list of courses
        val ad: ArrayAdapter<*> = ArrayAdapter<Any?>(
            this,
            android.R.layout.simple_spinner_item,
            difficulty)
        ad.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item)
        spin.adapter = ad

    }


}