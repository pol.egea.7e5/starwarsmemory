package com.example.starwarsmemory

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.starwarsmemory.R.*

class GameScreen : AppCompatActivity(){
    var scoreboard=0
    var animation=false
    var tempo: CountDownTimer? =null
    var counter=60
    var boolcomparer=false
    var cromos= mutableListOf(id.cromo1,id.cromo2,id.cromo3,id.cromo4,id.cromo5,id.cromo6,id.cromo7,id.cromo8)
    var imatges= mutableListOf(drawable.cromos1,drawable.cromos2,drawable.cromos3)
    var dificultat="Easy"
    var cromocomparer=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.gamescreen)

        if(intent?.extras?.getInt("score")!=null)scoreboard= intent?.extras?.getInt("score")!!
        if(intent?.extras?.getInt("time")!=null)counter= intent?.extras?.getInt("time")!!
        if(intent?.extras?.getString("dificultat")!=null)dificultat= intent?.extras?.getString("dificultat")!!
        println(counter)
        tempo=object : CountDownTimer((counter*1000).toLong(),1000){
            override fun onTick(p0: Long) {
                counter--
                findViewById<TextView>(id.tempo).text="$counter"
            }

            override fun onFinish() {
                val inte= Intent(this@GameScreen ,ResultScreen::class.java )
                inte.putExtra("marcador",scoreboard)
                startActivity(inte)
            }
        }.start()
        if(dificultat=="Easy"){
            findViewById<ImageView>(id.cromo7).setImageResource(android.R.color.transparent)
            findViewById<ImageView>(id.cromo7).isClickable=false
            findViewById<ImageView>(id.cromo8).setImageResource(android.R.color.transparent)
            findViewById<ImageView>(id.cromo8).isClickable=false
        }
        iniciJoc()
    }
    private fun iniciJoc(){
        findViewById<TextView>(id.scoreGameScreen).text="$scoreboard"
        if (dificultat=="Easy"){
            imatges.addAll(imatges)
            imatges.shuffle()
        }
        else {
            imatges.add(drawable.cromos4)
            imatges.addAll(imatges)
            imatges.shuffle()
        }
        for (obj in cromos){
            findViewById<ImageView>(obj).setOnClickListener{
                checkMemory(obj)
            }
        }

        findViewById<TextView>(id.tempo).text="$counter"


    }
    private fun checkMemory(obj:Int){
        if (!animation){
            val imgcanvi=imatges[cromos.indexOf(obj)]
            if(!boolcomparer){
                findViewById<ImageView>(obj).setImageResource(imgcanvi)
                findViewById<ImageView>(obj).isClickable=false
                cromocomparer=cromos.indexOf(obj)
                boolcomparer=true
            }
            else {
                findViewById<ImageView>(obj).setImageResource(imgcanvi)
                animation=true
                object : CountDownTimer(400,400){
                    override fun onTick(p0: Long) {
                    }

                    override fun onFinish() {
                        if(imatges[cromocomparer]==imatges[cromos.indexOf(obj)]){
                            scoreboard++
                            findViewById<ImageView>(obj).setImageResource(android.R.color.transparent)
                            findViewById<ImageView>(cromos[cromocomparer]).setImageResource(android.R.color.transparent)
                            findViewById<ImageView>(obj).isClickable=false
                            cromocomparer=0
                        }
                        else{
                            findViewById<ImageView>(obj).setImageResource(drawable.cara_posterior_cromos)
                            findViewById<ImageView>(cromos[cromocomparer]).setImageResource(drawable.cara_posterior_cromos)
                            findViewById<ImageView>(cromos[cromocomparer]).isClickable=true
                            cromocomparer=0
                        }
                        animation=false
                        checkGame()
                    }
                }.start()

                boolcomparer=false
            }
            findViewById<TextView>(id.scoreGameScreen).text="$scoreboard"
            }

    }
    fun checkGame(){
        if (dificultat!="Survival"){

            if (scoreboard==imatges.size/2){
                val inte= Intent(this ,ResultScreen::class.java )
                inte.putExtra("marcador",scoreboard)
                startActivity(inte)
                tempo?.cancel()
                finish()
            }

        }
        if (dificultat=="Survival"){

            if (scoreboard- (intent.extras?.getInt("score") as Int) ==imatges.size/2){
                val inte= Intent(this@GameScreen ,GameScreen::class.java )
                inte.putExtra("score",scoreboard)
                inte.putExtra("time",counter)
                inte.putExtra("dificultat",dificultat)
                startActivity(inte)
                tempo?.cancel()
                finish()
            }

        }
    }
    override fun onSaveInstanceState(savedInstanceState: Bundle) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        tempo?.cancel()
        savedInstanceState.putInt("time", counter)
        savedInstanceState.putInt("score", scoreboard)
        savedInstanceState.putString("dificultat", dificultat)
        savedInstanceState.putIntArray("imatges",imatges.toIntArray())
        savedInstanceState.putIntArray("cromos",cromos.toIntArray())
        val discoveredornone= mutableListOf<Boolean>()
        for (cromo in cromos){
            if (!findViewById<ImageView>(cromo).isClickable){
                if (dificultat=="Easy"&&(cromos.indexOf(cromo)==6||cromos.indexOf(cromo)==7))discoveredornone.add(false)
                else for (cromo2 in cromos){
                    if (dificultat=="Easy"&&(cromos.indexOf(cromo2)==6||cromos.indexOf(cromo2)==7))break
                    if (cromos.indexOf(cromo)!=cromos.indexOf(cromo2)&&imatges[cromos.indexOf(cromo)]==imatges[cromos.indexOf(cromo2)]&&!findViewById<ImageView>(cromo2).isClickable) discoveredornone.add(false)
                }

            }
            else discoveredornone.add(true)
        }
        savedInstanceState.putBooleanArray("discovered",discoveredornone.toBooleanArray())
        // etc.
        super.onSaveInstanceState(savedInstanceState)
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        counter=savedInstanceState.getInt("time")
        scoreboard=savedInstanceState.getInt("score")
        imatges= savedInstanceState.getIntArray("imatges")?.toMutableList()!!
        cromos= savedInstanceState.getIntArray("cromos")?.toMutableList()!!
        val checkvalues=savedInstanceState.getBooleanArray("discovered")!!
        for (cromo in cromos){
            if (!checkvalues[cromos.indexOf(cromo)]){
                findViewById<ImageView>(cromo).isClickable=false
                findViewById<ImageView>(cromo).setImageResource(android.R.color.transparent)
            }
        }
        tempo?.cancel()
        tempo=object : CountDownTimer((counter*1000).toLong(),1000){
            override fun onTick(p0: Long) {
                counter--
                findViewById<TextView>(id.tempo).text="$counter"
            }

            override fun onFinish() {
                val inte= Intent(this@GameScreen ,ResultScreen::class.java )
                inte.putExtra("marcador",scoreboard)
                startActivity(inte)
            }
        }.start()
        findViewById<TextView>(id.scoreGameScreen).text="$scoreboard"
    }


}