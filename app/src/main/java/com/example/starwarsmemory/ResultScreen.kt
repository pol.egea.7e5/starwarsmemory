package com.example.starwarsmemory

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import android.view.Menu
import android.view.MenuItem
import com.example.starwarsmemory.databinding.ActivityMainBinding
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener

class ResultScreen : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.resultscreen)
        val goMenuClick = findViewById<Button>(R.id.menuButton)
        goMenuClick.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        val exitClick = findViewById<Button>(R.id.exitButton)
        exitClick.setOnClickListener {
            finishAffinity()
        }
        findViewById<TextView>(R.id.score).text=intent?.extras?.getInt("marcador").toString()
    }
    override fun onSaveInstanceState(savedInstanceState: Bundle) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putString("score", findViewById<TextView>(R.id.score).text as String?)
        super.onSaveInstanceState(savedInstanceState)
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        findViewById<TextView>(R.id.score).text=savedInstanceState.getString("score")
    }

}